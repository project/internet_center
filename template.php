<?php

function internet_center_login_box() {
  global $user;
 
  if (!$user->uid) {
    $form['#action'] = url($_GET['q'], array('query' => drupal_get_destination()));
    $form['#id'] = 'user-login-form';
    $form['name'] = array('#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => 60,
      '#size' => 15,
      '#required' => TRUE,
    );
    $form['pass'] = array('#type' => 'password',
      '#title' => t('Password'),
      '#size' => 15,
      '#required' => TRUE,
    );
    $form['submit'] = array('#type' => 'submit',
      '#value' => t('Log in'),
    );

    if (variable_get('user_register', 1)) {
      $items[] = l(t('Create new account'), 'user/register', array('title' => t('Create a new user account.')));
    }
    $items[] = l(t('Request new password'), 'user/password', array('title' => t('Request new password via e-mail.')));
    $form['links'] = array('#value' => theme('item_list', $items));

    $output .= drupal_get_form('user_login_block', $form, 'user_login');

    return $output;
  }
  else {
    $output = '<p>You are logged in as <b>' . $user->name . '</b>';
    $output .= '&nbsp;&nbsp;&ndash;&nbsp;&nbsp;' . l(t('Logout'), 'logout', array('title' => t('Logout'))) . '<br>';
    $output .= l(t('Manage my account'), 'user/' . $user->uid . '', array('title' => t('manage my account')));
    $output .= '</p>';
    return $output;
  }
}

/* Checks to see if we're in the admin section */
function internet_center_is_admin() {
  if (((arg(0) == 'admin') || (arg(0) == 'administer') || (arg(0) == 'user') ) && (user_access('access administration pages'))) {
    return true;
  }
}


?>
